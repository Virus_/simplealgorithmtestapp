package SortingAlgorithmsv2;

import java.util.ArrayList;

public interface Sorting {

    ArrayList<Integer> sorting(int tab[]);

    // Interfejs sorting udostepniajacy mi gwarancje tego ze kazda klasa ktora go dziediczy bedzie musiala
    // go zaimplementowac, dzieki czemu uzyskam gwarancje tego ze kazdy algorytm bedzie sortowal dane w zupelnie inny sposob, zastosowalem tutaj polimorfizm,
    // czyli wykorzystywanie wartosci, zmiennych i metod  w wielokrotny sposob. W tym wypadku metody.



}
