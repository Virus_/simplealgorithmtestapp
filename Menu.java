package SortingAlgorithmsv2;

import javax.xml.crypto.Data;
import java.lang.management.GarbageCollectorMXBean;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

import static SortingAlgorithmsv2.Main.*;
public abstract class Menu extends SortingAlgorithms {
    static SortingAlgorithms algorithms;
    static String choice;

    static AlgorithmType type;
    static void menuOfProgram() throws Exception {

        menu();
        algorithms = createAlgorithm(AlgorithmType.values()[number]);
        algorithms.run();
    }


    static String getString() {
        return new Scanner(System.in).nextLine();
    }


}

