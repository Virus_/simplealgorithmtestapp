package SortingAlgorithmsv2;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Scanner;

public abstract class SortingAlgorithms implements Sorting, Run, AlgorithmTime {

    String algorithmName;
    AlgorithmType type;
    static int number;
    static int entered = 0;

    int difficultyOfAlgorithm;

    static int numberOfElement;  // zmienna ktora przechowuje ilosc elementow tablicy,
    static int valueOfElement; // zmienna ktora ustawia zakres wartosci.
    final static int ZERO = 0, ONE = 1, TWO = 2, THREE = 3, FOUR = 4, FIVE = 5, SIX = 0, SEVEN = 0, EIGHT = 0, NINE = 0, TEEN = 0;

    public void setType(AlgorithmType type) {
        this.type = type;
    }

    public AlgorithmType getType() {
        return type;
    }

    public void setAlgorithmName(String algorithmName) {
        this.algorithmName = algorithmName;
    }

    public String getAlgorithmName() {
        return algorithmName;
    }

    static SortingAlgorithms createAlgorithm(AlgorithmType type) { // statyczna funkcja ktora zwraca mi referencje do obiektu ktory zostanie utworzony
        // na podstawie wartosci wprowadzonej przez uzytkownika, dzieki temu zaoszedzie czasu i lini kodu w czasie implementacji.
        // W dodatku kazdy nowy obiekt bedzie typu algorithms ale dzialanie poszczegolnych algorytmow bedzie indywidualne zalezne od implementacji
        // czyli wykorzsytuje tutaj korzysci wynikajace z dziedziczenia.

        SortingAlgorithms algorithms = null;
        switch (type) {
            case BUBBLESORT -> algorithms = new BubbleSort() {
            };
        }
        return algorithms;
    }

    static int[] createArray(int elementOfArray, int valueOfNumber) {
        int i = 0, randomArray[] = new int[elementOfArray];
        SecureRandom random = new SecureRandom();
        for (int counter : randomArray) {
            randomArray[i] = +ONE + random.nextInt(valueOfNumber);
            i++;
        }
        return randomArray;

    }

    static void menu() throws Exception {
        System.out.print("<======| SortingAlgorithms |======>\n");
        System.out.println("1.Algorithms list\n");
        switch (exception()) { // jako argument
            // podaje funkcje ktora pobiera dane z klawiatury,
            // i  zwraca wartosc tylko i tylko wtedy kiedy jest ona wartoscia int.
            case ONE -> {
                System.out.println("0.Bubble sort");
                System.out.println("1.Counting sort");
                switch (exception()) {
                    case ZERO, ONE, TWO, THREE, FOUR, FIVE -> {
                        System.out.println("Enter the number of array elements.");
                        numberOfElement = exception();
                        System.out.println("Podaj zakres wartosci dla losowanych elementow");
                        valueOfElement = exception();
                    }

                }

            }
        }

    }


    static String getString() {
        return new Scanner(System.in).nextLine();
    }

    static int getInt() throws Exception {
        return new Scanner(System.in).nextInt();
    }

    static void ptintCollection(int tab[]) {
        for (int g : tab) {
            System.out.print(g);
            System.out.print(' ');
        }

    } // Metody sa statyczne bo beda wykorzystywane przez wszystkie obiekty w ten sam sposob, wiec nie mam potrzeby aby

    // byly abstrakcyjne i implementowane idywidualnie, w dodatku przeciazam je aby mogly przyjmowac i tablice i liste jako parametr.
    static void printCollection(ArrayList<Integer> list) {
        for (int g : list) {
            System.out.print(g);
            System.out.print(' ');
        }
    }

   static int exception() {
        int temporaryVariable = 0;
       try {
           temporaryVariable = getInt();
       } catch (Exception e) {
           System.out.println("Wrong value,try again.");
           exception();
       }
       return temporaryVariable;
   }

    public double AlgorithmUpTime(int[] array) {

        // TODO: 08/06/2022 1. Uogolnic metode, uwydajnic proces;
        // TODO: 08/06/2022 2. Zaimplementowac meetode w klasie nadrzednej

        Date start = new Date();
        sorting(array);
        Date end = new Date();
        double value = start.getTime();
        double value1 = end.getTime();
        double endValue = value1 - value;


        return endValue / 1000;

        // 1. Metoda dziala poprawnie lecz jest niewydajna, z tego wzgledu ze program wykonuje proces sortowania dla
        // wypisania danych do konsoli a pozniej przetwarza jeszcze raz ten sam proces dla zwrocenia czasu dzialania
        // algoruytmu, wydluza to dzialanie procesu  az o 100 %, przyjrzec sie temu, ugolonic bardziej ta metode.

        // 2. Doszedlem do wniosku ze metoda Time, powinna zostac umiejscowioan w klasie nadrzednej, i
        // powinna wywolywac metode sort, zaleznie od przyjmowanego argumentu tzn instancji obiektu klas podrzednych
        // dzieki temu nie bede musial implementowac tej metody i powielac kodu w klasach podrzednych.


    } //



}



