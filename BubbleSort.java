package SortingAlgorithmsv2;

import java.util.ArrayList;
import java.util.Date;

import static SortingAlgorithmsv2.Main.getInt;

public class BubbleSort extends SortingAlgorithms {
    BubbleSort() {

        super.setAlgorithmName("BubbleSort");
        super.setType(AlgorithmType.BUBBLESORT);
    }

    @Override
    public ArrayList<Integer> sorting(int[] tab) { // interfejs odpowiedzialny
        // za udostepnianie uzytkownikowi funckji sortowania babalekowego, zastosowany polimorfizm.

        ArrayList<Integer> list = new ArrayList<>();
        for ( int i = ZERO ; i < tab.length ; i++ ) {
            for ( int j = ZERO ; j < tab.length - 1 ; j++ ) {
                if (tab[j] > tab[j + ONE]) {
                    int sum = tab[j + ONE];
                    tab[j + ONE] = tab[j];
                    tab[j] = sum;
                }
            }
        }

        for (int j = 0; j < tab.length; j++) {
            list.add(tab[j]);}
        return list;
    }
    @Override
    public void run() {
        ArrayList<Integer> list = new ArrayList<>();
        int array[] = createArray(numberOfElement, valueOfElement);
        System.out.println("Dane przed sortowaniem\n");
        ptintCollection(array);
        System.out.println("\n");
        System.out.println("Dane po sortowaniu\n");
        printCollection(sorting(array));
        System.out.println("Duration of Algorithm :" + " " + AlgorithmUpTime(array) + " " + "seconds");

    }


}



